## Introdution

List of functions:

```swift
class func setupStack(configuration: Configuration, removeDBOnSetupFailed: Bool) throws -> PoliteCoreStorage

func resetMainQueueContext()
func resetRootSavingContext()

// Save/Create
func saveWithBlock(_ block: @escaping (_ context: NSManagedObjectContext) -> Void, completion: @escaping ((_ error: Error?) -> Void))

/// Synchronous variant of [saveWithBlock](x-source-tag://saveWithBlock)
/// - Tag: saveWithBlockAndWait
func saveWithBlockAndWait(_ block: @escaping (_ context: NSManagedObjectContext) -> Void)

func findFirstOrCreate<T: NSManagedObject>(_ entityType: T.Type, withPredicate predicate: NSPredicate, inContext context: NSManagedObjectContext)

func findFirstByIdOrCreate<T: NSManagedObject>(_ entityType: T.Type,
                                                  identifier: PredicateConvertible,
                                                  inContext context: NSManagedObjectContext,
                                                  andPredicateFormat format: String? = nil,
                                                  argumentArray: [Any]? = nil) -> T
                                                  
func findFirstById<T: NSManagedObject>(_ entityType: T.Type,
                                         identifier: PredicateConvertible,
                                         inContext context: NSManagedObjectContext,
                                         andPredicateFormat format: String? = nil,
                                         argumentArray: [Any]? = nil) -> T?
                                         
// MARK: Main Queue

func existingObjectWithIDInMainQueueContext<T: NSManagedObject>(_ objectID: NSManagedObjectID) -> T?
func findFirstInMainQueueContext<T: NSManagedObject>(_ entityType: T.Type, withPredicate predicate: NSPredicate) -> T?
func findAllInMainQueueContext<T: NSManagedObject>(_ entityType: T.Type, sortTerm: [(sortKey: String, ascending: Bool)] = [], predicate: NSPredicate? = nil) -> [T]?

func mainQueueFetchedResultsController<T: NSManagedObject>(_ entityType: T.Type,
                                                             sortDescriptors: [NSSortDescriptor],
                                                             predicate: NSPredicate? = nil,
                                                             sectionNameKeyPath: String? = nil,
                                                             cacheName: String? = nil,
                                                             configureRequest: ((_ request: NSFetchRequest<T>) -> Void)?) -> NSFetchedResultsController<T>
                                                             
func countForEntityInMainQueueContext<T: NSManagedObject>(_ entityType: T.Type, predicate: NSPredicate? = nil) -> Int

// MARK: General

func fetchWithBlock(_ block: @escaping ((_ context: NSManagedObjectContext) -> Void), waitUntilFinished: Bool)
func existingObjectWithID<T: NSManagedObject>(_ objectID: NSManagedObjectID, inContext context: NSManagedObjectContext) -> T?
func findFirst<T: NSManagedObject>(_ entityType: T.Type, withPredicate predicate: NSPredicate, inContext context: NSManagedObjectContext) -> T?

func findAll<T: NSManagedObject>(_ entityType: T.Type,
                                   inContext context: NSManagedObjectContext,
                                   sortTerm: [(sortKey: String, ascending: Bool)] = [],
                                   predicate: NSPredicate? = nil) -> [T]?
                                   
func countForEntity<T: NSManagedObject>(_ entityType: T.Type, inContext context: NSManagedObjectContext, predicate: NSPredicate? = nil) -> Int
```
