## FetchedResultsController

Wrapper on NSFetchedResultsController, provides easy way to observe collection of entities.

```swift
func mainQueueFetchedResultsController<T: NSManagedObject>(_ entityType: T.Type,
                                                             sortDescriptors: [NSSortDescriptor],
                                                             predicate: NSPredicate? = nil,
                                                             sectionNameKeyPath: String? = nil,
                                                             cacheName: String? = nil,
                                                             configureRequest: ((_ request: NSFetchRequest<T>) -> Void)?) -> NSFetchedResultsController<T>
```

Notifies that section and object changes are about to be processed and notifications will be sent. Is equivalent to NSFetchedResultsControllerDelegate controllerWillChangeContent:

```swift
public var willChangeContent: ((_ controller: FetchedResultsController<EntityType, ResultType>) -> Void)?
```

Notifies that all section and object changes have been sent. Is equivalent to NSFetchedResultsControllerDelegate controllerDidChangeContent:

```swift
public var didChangeContent: ((_ controller: FetchedResultsController<EntityType, ResultType>) -> Void)?
```
Notifies about particular changes such as add, remove, move, or update:
 
```swift
public var didChangeFetchedResults: ((_ controller: FetchedResultsController<EntityType, ResultType>, _ type: FetchedResultsControllerChangeType) -> Void)?
```

Wrapped NSFetchedResultsController:

```swift
public let fetchedResultsController: NSFetchedResultsController<EntityType>
public init(fetchedResultsController: NSFetchedResultsController<EntityType>) 
```

Set new sort descriptors. Call performFetch() to apply.  Pass true "shouldPerformFetch" to perform fetch at the end of the method execution:

```swift
public func setSortDescriptors(_ sortDescriptors: [NSSortDescriptor], shouldPerformFetch: Bool) 
```

Calls performFetch() method of NSFetchedResultsController:

```swift
public func performFetch() throws
```

Sets new predicate, deletes cache and calls performFetch() method of NSFetchedResultsController

```swift
public func performFetch(predicate: NSPredicate) throws
```

Computes total number of items across all sections. Returns total number of items:

```swift
public func totalNumberOfItems() -> Int 
```

Total number of items in section:

```swift
public func numberOfItemsInSection(_ index: Int) -> Int
```

Number of sections:

```swift
public func numberOfSections() -> Int
```

Fetched entity at a given indexPath:

```swift
public func itemAtIndexPath(_ indexPath: IndexPath) -> ResultType
```

Generic version of [func itemAtIndexPath(_ indexPath: IndexPath) -> ResultType](x-source-tag://funcItemAtIndexPathTyped)

```swift
public func itemAtIndexPath<T: ManagedEntity>(_ indexPath: IndexPath) -> T?
```

IndexPath for the specified entity or nil if the entity does not exist:

```swift
public func indexPath(entity: ResultType) -> IndexPath?
```

Entity for the specified URI representation of an object ID or nil if the object does not exist:

```swift
public func itemWithURL(_ url: URL) -> ResultType?
```

Calls the given closure on each element in the section specified by index,  body - closure that takes an entity and index path from the section as a parameters.

```swift
public func forEach(inSection section: Int, body: (IndexPath, ResultType) -> Bool)
```

## SingleObjectFetchedResultController

Wrapper on NSFetchedResultsController, provides easy way to observe single entity.

## Usage

```swift
let exampleFetchedResultController = FetchedResultsController<CDExampleEntity, ManagedExampleEntity>(fetchedResultsController: controller)

exampleFetchedResultController.willChangeContent = { (_) in }

exampleFetchedResultController.didChangeFetchedResults = {[weak self] (controller, changeType) in
    guard let actualSelf = self else {
        return
    }
    //do something
}
exampleFetchedResultController.didChangeContent = {[weak self] (controller) in
    guard let actualSelf = self else {
        return
    }
    //do something
}
do {
    try exampleFetchedResultController.performFetch()
} catch let error {
    assertionFailure("\(type(of: self)) - \(#function): . \(error)")
}
```
