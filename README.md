# PoliteCoreData

![PoliteCoreData](title_image.png)
<br><br>

![Version](https://img.shields.io/badge/version-1.0.0-blue.svg)
![Platform](https://img.shields.io/badge/platform-iOS-lightgrey.svg)
![License MIT](https://img.shields.io/badge/license-MIT-green.svg)

- [Requirements](#requirements)
- [Introdution](pcddoc/introdution.md)
- [Quick start](#quick-start)
- [Find or Create Entity ](#find-or-create-entity)
- [Fetching Entities](#fetching-entities)
- [Update Entity](#update-entity)
- [Delete Entity](#delete-entity)
- [FetchedResultsController](pcddoc/fetched_results_controller.md)
- [Installation](#installation)
- [License](#license)

## Quick start

PoliteCoreData - the main object that manages Core Data stack and encapsulates helper methods for interaction with Core Data objects

1.  If you're using CocoaPods  you should import:

 ```swift
 import PoliteCoreData
```

2. Somewhere in your app delegate, in either the - applicationDidFinishLaunching: withOptions: method, use one of the following setup calls with the PoliteCoreData class:

Creates PoliteCoreStorage instance, according to given configuration:

 ```swift
class func setupStack(configuration: Configuration, removeDBOnSetupFailed: Bool) throws -> PoliteCoreStorage

//Where Configuration
/// Encapsulates initial setup parameters
/// - Tag: PoliteCoreStorage.Configuration
public struct Configuration {

    /// The name of .xcdatamodeld file
    public let modelName: String

    /// Initializes Configuration
    ///
    /// - Parameter modelName: The name of .xcdatamodeld file
    public init(modelName: String) {
        self.modelName = modelName
    }
}
 ```

## Find or Create Entity

To create and insert a new instance of an Entity in context, you can use:

```swift
func findFirstOrCreate<T: NSManagedObject>(_ entityType: T.Type, withPredicate predicate: NSPredicate, inContext context: NSManagedObjectContext)
func findFirstByIdOrCreate<T: NSManagedObject>(_ entityType: T.Type,
                                                  identifier: PredicateConvertible,
                                                  inContext context: NSManagedObjectContext,
                                                  andPredicateFormat format: String? = nil,
                                                  argumentArray: [Any]? = nil) -> T
                                                  
// or in main context
func findFirstInMainQueueContext<T: NSManagedObject>(_ entityType: T.Type, withPredicate predicate: NSPredicate) -> T?

//sample
private var storage: PoliteCoreStorage = {
    do {
        return try PoliteCoreStorage.setupStack(configuration: PoliteCoreStorage.Configuration(modelName: "PoliteCoreDataExample"), removeDBOnSetupFailed: true)
    } catch let error {
        fatalError("\(error)")
    }
}()

func add() {
    let coreStorage = storage
    coreStorage.save({ (context) in
        let notManagedEntity = ExampleEntity(identifier: UUID().uuidString, createdAt: Date(), updatedAt: Date())
        let newEntity = coreStorage.findFirstByIdOrCreate(CDExampleEntity.self, identifier: notManagedEntity.identifier, inContext: context)
    }, completion: { (error) in
        if let actualError = error {
            assertionFailure("\(actualError)")
        }
    })
}
```

## Fetching Entities

Finds all entities with given type. Optionally filterred by predicate (list of available functions in [introdution](pcddoc/introdution.md)):

```swift
func findFirstInMainQueueContext<T: NSManagedObject>(_ entityType: T.Type, withPredicate predicate: NSPredicate) -> T?

// or
func findAll<T: NSManagedObject>(_ entityType: T.Type,
                                   inContext context: NSManagedObjectContext,
                                   sortTerm: [(sortKey: String, ascending: Bool)] = [],
                                   predicate: NSPredicate? = nil) -> [T]?
                                   
//sample

let exampleEntities = coreStorage.findAll(CDExampleEntity.self, inContext: context)
```

## Update Entity

1. Implement update function in your entity object:

```swift
extension CDExampleEntity {
    func update(entity: ExampleEntity) -> Bool {
        guard isInserted || (entity.identifier == identifier) else {
            return false
        }
        var changed: Bool = false
        changed = apply(path: \.identifier, value: entity.identifier) || changed
        changed = apply(path: \.createdAt, value: entity.createdAt.timeIntervalSince1970) || changed
        changed = apply(path: \.updatedAt, value: entity.updatedAt.timeIntervalSince1970) || changed

        return changed
    }

    func apply<Value>(path: ReferenceWritableKeyPath<CDExampleEntity, Value?>, value: Value?) -> Bool where Value: Equatable {
        return NSManagedObject.applyValue(to: self, path: path, value: value)
    }

    func apply<Value>(path: ReferenceWritableKeyPath<CDExampleEntity, Value>, value: Value) -> Bool where Value: Equatable {
        return NSManagedObject.applyValue(to: self, path: path, value: value)
    }
}
```
2. Call method in closure that takes a context as a parameter. Will be executed on private context queue. Caller could apply any changes to DB in it. At the end of execution context will be saved.

```swift
func updateItem() {
    let coreStorage = storage
    coreStorage.save({ (context) in
        let notManagedEntity = ExampleEntity(identifier: UUID().uuidString, createdAt: Date(), updatedAt: Date())
        let newEntity = coreStorage.findFirstByIdOrCreate(CDExampleEntity.self, identifier: notManagedEntity.identifier, inContext: context)
        _ = newEntity.update(entity: notManagedEntity)
    }, completion: { (error) in
        if let actualError = error {
            assertionFailure("\(actualError)")
        }
    })
}
```

## Delete Entity

```swift
func deleteItem(at indexPath: IndexPath) {
    let coreStorage = storage
    let item = exampleFetchedResultController.itemAtIndexPath(indexPath)
    coreStorage.save({ (context) in
        if let entity = coreStorage.findFirstById(CDExampleEntity.self, identifier: item.identifier, inContext: context) {
            context.delete(entity)
        }
    }, completion: { (error) in
        if let actualError = error {
            assertionFailure("\(actualError)")
        }
    })
}
```

## Requirements

- iOS 10.0+
- Xcode 9.2+
- Swift 4.0+

## Installation

### CocoaPods

[CocoaPods](http://cocoapods.org) is a dependency manager for Cocoa projects. You can install it with the following command:

```bash
$ gem install cocoapods
```

To integrate Toolbox into your Xcode project, specify it in your `Podfile`:

```ruby
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '10.0'
use_frameworks!

target '<Your Target Name>' do
    pod 'Shakuro.PoliteCoreData', :git => 'https://gitlab.com/shakuro-public/politecoredata', :tag => '1.0.0'
end
```

Then, run the following command:

```bash
$ pod install
```

### Manually

If you prefer not to use CocoaPods, you can integrate any/all components from the Shakuro iOS Toolbox simply by copying them to your project.

## License

Shakuro iOS Toolbox is released under the MIT license. [See LICENSE](https://github.com/shakurocom/iOS_Toolbox/blob/master/LICENSE) for details.# Polite Core Data
